import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PredkoscSrednia {

    public static void main(String[] args) {

        //program napisany w Javie nie przechodzi testu na stronie spoj
        //poniewaz wymagany czas to 0.1s, natomiast program kompiluje się w 0.13s
        //dlatego przepisalem go na C# (kod ponizej, zakomentowany, zaakceptowany na spoj)

        int t;
        Scanner sc = new Scanner(System.in);
        t = sc.nextInt();

        //create list to save values of speeds
        List<Integer> listOfSpeeds = new ArrayList<>();

        //add values to listOfSpeed
        for (int i = 0; i < t; i++) {
            int v1 = sc.nextInt();
            int v2 = sc.nextInt();
            listOfSpeeds.add(v1);
            listOfSpeeds.add(v2);
        }

        for (int i = 0; i < listOfSpeeds.size(); i += 2) {
            //counting and print average speed
            int vsr = (2*listOfSpeeds.get(i)*listOfSpeeds.get(i+1)/(listOfSpeeds.get(i)+listOfSpeeds.get(i+1)));
            System.out.println(vsr);
        }
    }
}

// przepisany powyzszy kod z Javy na C#, zeby przeszedl test na stronie spoj

//    using System;
//    using System.Collections.Generic;
//
//public class Test
//{
//    public static void Main()
//    {
//        int t = Convert.ToInt32(Console.ReadLine());
//
//        //create list to save values
//        List<int> listOfSpeeds = new List<int>();
//
//
//        for (int i = 0; i < t; i++) {
//            //read values
//            string s = Console.ReadLine();
//            string[] values = s.Split(' ');
//            int v1 = int.Parse(values[0]);
//            int v2 = int.Parse(values[1]);
//
//            //add values to list
//            listOfSpeeds.Add(v1);
//            listOfSpeeds.Add(v2);
//        }
//
//        for (int i = 0; i < listOfSpeeds.Count; i += 2) {
//
//            //counting average speed
//            int vsr = (2*listOfSpeeds[i]*listOfSpeeds[i+1]/(listOfSpeeds[i]+listOfSpeeds[i+1]));
//            //print average speed
//            Console.WriteLine(vsr);
//
//        }
//    }
//}
