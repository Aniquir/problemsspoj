import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Skarbfi {

    public static void main(String[] args) {

        // link do zadania: http://pl.spoj.com/problems/SKARBFI/
        //program wyswietla poprawny wynik (dla wartosci podanych w zadaniu),
        // jednak nie przechodzi na spoj (błędna odpowiedź)
        //przechodzi również na stronie ideone.com
        //link do ideone: https://ideone.com/FcKXYV

        int D;
        Scanner sc = new Scanner(System.in);
        D = sc.nextInt();

        List<Integer> dataForGems = new LinkedList<>();

            for (int i = 0; i < D; i++) {
                int x = 0;
                int y = 0;
                int N;
                N = sc.nextInt();
                for (int j = 0; j < N; j++) {
                    int a = sc.nextInt();
                    int b = sc.nextInt();
                    dataForGems.add(a);
                    dataForGems.add(b);
                }
                for (int k = 0; k < dataForGems.size(); k += 2) {

                    if (dataForGems.get(k) == 0) {
                        x += dataForGems.get(k + 1);
                    }
                    if (dataForGems.get(k) == 1) {
                        x -= dataForGems.get(k + 1);
                    }
                    if (dataForGems.get(k) == 2) {
                        y += dataForGems.get(k + 1);
                    }
                    if (dataForGems.get(k) == 3) {
                        y -= dataForGems.get(k + 1);
                    }
                }
                for (int m = 0; m < dataForGems.size(); m++) {
                    if (x == 0 && y == 0) {
                        System.out.println("studnia");
                    } else {
                        if (x > 0) {
                            System.out.println(0 + " " + x);
                            if (y == 0){
                                break;
                            }
                        } else {
                            x *= -1;
                            System.out.println(1 + " " + x);
                            if (y == 0){
                                break;
                            }
                        }
                        if (y > 0) {
                            System.out.println(2 + " " + y);
                            if (x == 0){
                                break;
                            }
                        } else {
                            y *= -1;
                            System.out.println(3 + " " + y);
                            if (x == 0){
                                break;
                            }
                        }
                    }
                    break;
                }
                dataForGems.clear();
            }
    }
}
