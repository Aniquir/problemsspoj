import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Przedszkolanka {

    public static void main(String[] args) {

        int N;
        Scanner sc = new Scanner(System.in);
        N = sc.nextInt();

        //create list to save values
        List<Integer> candyList = new LinkedList<>();

        //add values to list
        if (N != 0) {
            for (int i = 0; i < N; i++) {
                int firstSet = sc.nextInt();
                int secondSet = sc.nextInt();
                candyList.add(firstSet);
                candyList.add(secondSet);
            }
        }

        //loop with conditions to check smallest common divisor
        for (int i = 0; i < candyList.size(); i += 2) {
            int numberOfCandies;
            if (candyList.get(i) > candyList.get(i+1)){
                numberOfCandies = candyList.get(i);
                for (int j = 0; j < j+1; j++) {
                    if (numberOfCandies % candyList.get(i + 1) == 0 && numberOfCandies % candyList.get(i) == 0) {
                        System.out.println(numberOfCandies);
                        break;
                    }
                    numberOfCandies++;
                }
            } else {
                numberOfCandies = candyList.get(i + 1);
                for (int j = 0; j < j + 1; j++) {
                    if (numberOfCandies % candyList.get(i) == 0 && numberOfCandies % candyList.get(i + 1) == 0) {
                        System.out.println(numberOfCandies);
                        break;
                    }
                    numberOfCandies++;
                }
            }
        }
    }
}
