import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Flamaster {

    public static void main(String[] args) {

        int C;
        Scanner sc = new Scanner(System.in);
        C = sc.nextInt();

        // create list to save values
        List<String> strings = new LinkedList<>();

        //add values to list
        if (C != 0) {
            for (int i = 0; i < C; i++) {
                String a = sc.next();
                strings.add(a);
            }
        }

        int count = 1;
        for (int i = 0; i < strings.size(); i++) {

            //created array for separating, comparing and counting values
            //separating
            String[] splitStrings = strings.get(i).split("");

            for (int j = 0; j < splitStrings.length; j++) {
                if (j == splitStrings.length - 1) {
                    //counting
                    count = getCount(count, splitStrings, j);
                } else {
                    //comparing
                    if (splitStrings[j].equals(splitStrings[j + 1])) {
                        count++;
                    } else {
                        //counting
                        count = getCount(count, splitStrings, j);
                    }
                }
            }
            System.out.println();
        }
    }

    //method to count and print values, created because it's used more than once
    private static int getCount(int count, String[] splitStrings, int j) {
        if (count == 1) {
            System.out.print(splitStrings[j]);
            count = 1;
        } else {
            if (count == 2) {
                System.out.print(splitStrings[j] + splitStrings[j]);
                count = 1;
            } else {
                System.out.print(splitStrings[j] + count);
                count = 1;
            }
        }
        return count;
    }
}
