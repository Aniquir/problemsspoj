import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class NajwiekszyWspolnyDzielnik {

    public static void main(String[] args) {

        int t;
        Scanner sc = new Scanner(System.in);
        t = sc.nextInt();

        //create list to save values, which program use
        List<Integer> values = new LinkedList<>();

        //add values to list, t - number of times
        if (t != 0) {
            for (int i = 0; i < t; i++) {
                int a = sc.nextInt();
                int b = sc.nextInt();
                values.add(a);
                values.add(b);
            }
        }

        //using function 'nwd' for values from list
        for (int i = 0; i < values.size(); i += 2){
            nwd(values.get(i), values.get(i+1));
        }
    }
    //function nwd
    static int nwd(int a, int b){
        int c=1;

        if (a == 0){
            System.out.println(b);
            return b;
        }else if (b == 0){
            System.out.println(a);
            return a;
        }else{
            while (c != 0){
                c = 1;
                c = a % b;
                a = b;
                b = c;
            }
            System.out.println(a);
            return a;
        }
    }
}

