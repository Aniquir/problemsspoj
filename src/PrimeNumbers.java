import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class PrimeNumbers {

    public static void main(String[] args) {

        int n;
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();

        //create list to save values to check
        List<Integer> values = new LinkedList<>();

        //add values to list
        if (n != 0) {
            for (int i = 0; i < n; i++) {
                values.add(sc.nextInt());
            }
        }

        //two loops with check values from list and print function the answer
        for (int value : values) {
            boolean isPrime = false;
            //condition if values is two
            if (value == 2) {
                isPrime = true;
            } else {
                for (int j = 2; j < value; j++) {
                    if (value % j == 0) {
                        isPrime = false;
                        break;
                    } else {
                        isPrime = true;
                    }
                }
            }
            if (isPrime) {
                System.out.println("TAK");
            } else {
                System.out.println("NIE");
            }
        }
    }
}
